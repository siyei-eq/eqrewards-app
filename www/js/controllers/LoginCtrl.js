Ctrl.controller('LoginCtrl', function($scope, $state, UserService, $ionicHistory) {

    $scope.userInfo = {};
    $scope.loginData = {};
    $scope.registerData = {};
    $scope.productData = {};
    $scope.forgotpassData = {};

    $scope.doLoginPage = function(loginForm){
        if (!loginForm.$valid) {
            return false;
        }
        UserService.login($scope.loginData).then(function(response) {
            if(response === true){
                //$scope.closeModalLogin();
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go("tab.home");
            }
        });
    };

    $scope.doLoginWithFacebook2 = function() {
        //AuthService.loginWithFacebook($scope);
L
    };

    var lastEmailUsedInLoggin = window.localStorage.getItem("last_email_login");
    if( lastEmailUsedInLoggin !== null && lastEmailUsedInLoggin !== undefined ){
        $scope.loginData.user_login = lastEmailUsedInLoggin;
    }

    $scope.isLoggedIn = function() {
        if (UserService.isLoggedIn()) {
            $scope.userInfo = UserService.getUser();
            return true;
        }
        else {
            return false;
        }
    };


});