Ctrl.controller('OrdersCtrl', function($scope, $state, UserService, OrderService) {
    $scope.listOrderData = [];
    $scope.pageOrder = 1;
    $scope.canLoadMoreOrderData = false;
    $scope.openSingleOrder = function($orderId) {
        angular.forEach($scope.listOrderData, function(order, key) {
            if (order.id === $orderId) {
                window.localStorage.setItem("singleOrder", JSON.stringify(order));
                $state.go('tab.order', {orderId: $orderId});
                return true;
            }
        });
    };
    $scope.getListOrderData = function($page) {
        OrderService.getListOrder(UserService.getAccessToken(),$page).then(function(listOrder) {
            if (listOrder.length > 0) {
                //update sample html to root
                angular.forEach(listOrder, function(order, key) {
                    $scope.listOrderData.push(order);
                });
                $scope.pageOrder = $page + 1;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                $scope.canLoadMoreOrderData = true;
            }
            else {
                $scope.canLoadMoreOrderData = false;
            }

        });
    };
    $scope.loadMoreListOrderData = function() {
        $scope.getListOrderData($scope.pageOrder);
    };
    $scope.getListOrderData($scope.pageOrder);
});