Ctrl.controller('CartCtrl', function($scope, $stateParams, $state, $timeout, UserService, CartService, AnimationService) {
    $scope.cartInfo = CartService.getCartInfo();
    $scope.cartQuantity = CartService.getCartQuantity();
    $scope.cartTotalPoints = CartService.getCartTotalPoints();
    $scope.cartTotalMoney = CartService.getCartTotalMoney();
    $scope.cartEmpty = false;
    $scope.cartUpdatedProduct = {};

    if($scope.cartQuantity == 0){
        $scope.cartEmpty = true;
    }

    $scope.openCheckout = function() {
        if( UserService.isLoggedIn() ) {
            $state.go('tab.checkout-billing');
        }
        else {
            $state.go('tab.checkout');
        }
    };

    $scope.removeProductFromCart = function($productId, $price_type) {
        AnimationService.action('cart_item_'+$productId+"-"+$price_type, 'bounceOut', 2000);
        $timeout(function(){
            CartService.removeProductFromCart($productId, $price_type);
            $scope.cartInfo = CartService.getCartInfo();
            $scope.cartQuantity = CartService.getCartQuantity();
            //$scope.cartTotal = CartService.getCartTotal();
            $scope.cartTotalPoints = CartService.getCartTotalPoints();
            $scope.cartTotalMoney = CartService.getCartTotalMoney();

            if($scope.cartQuantity==0){
                $timeout(function(){
                    $scope.cartEmpty = true;
                },600);
            }

        },200);

    };

    $scope.updateCart = function ($productId, $price_type) {
        //CartService.updateCart($productId, $price_type, $scope.cartUpdatedProduct[$productId]);
        console.debug($scope.cartUpdatedProduct);
        CartService.updateCart($productId, $price_type, $scope.cartUpdatedProduct[$price_type+''+$productId]);
        $scope.cartInfo = CartService.getCartInfo();
        $scope.cartQuantity = CartService.getCartQuantity();
        //$scope.cartTotal = CartService.getCartTotal();
        $scope.cartTotalPoints = CartService.getCartTotalPoints();
        $scope.cartTotalMoney = CartService.getCartTotalMoney();
    };

    $scope.$on("$ionicView.afterEnter", function(event, data){
        var $elId;
        angular.forEach($scope.cartInfo.products, function(item, k){
            //realQuantity < requestedQuantity
            if( item[2].quantity < item[1] && item[2].quantity != -1 ){
                $elId = 'cart_item_'+item[0]+"-"+item[2].price_type;
                AnimationService.action($elId, 'flash', 2000);
            }
        });
    });

});