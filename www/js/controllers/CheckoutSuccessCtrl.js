Ctrl.controller('CheckoutSuccessCtrl', function($scope, $state, $rootScope, $ionicHistory, OrderService, UserService) {
    $scope.orderReceivedInfo = OrderService.getOrderReceivedInfo();
    $scope.subTotal = 0;
    angular.forEach($scope.orderReceivedInfo.line_items, function(product, key) {
        $scope.subTotal = $scope.subTotal + parseFloat(product.subtotal);
    });

    $scope.$on("$ionicView.beforeEnter", function(event, data){
        $scope.orderReceivedInfo = OrderService.getOrderReceivedInfo();
        $scope.subTotal = 0;
        angular.forEach($scope.orderReceivedInfo.line_items, function(product, key) {
            $scope.subTotal = $scope.subTotal + parseFloat(product.subtotal);
        });

        UserService.updateUserData(false);
    });

    $scope.goHomePage = function() {
        OrderService.updateOrderReceivedInfo({});
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $ionicHistory.clearCache();
        $rootScope.hideTabs = '';
        $state.go('tab.home');
    };
});