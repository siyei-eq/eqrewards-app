Ctrl.controller('CategoriesCtrl', function($scope, $state, $stateParams, $rootScope, ListCategoryService, AppService) {
    var settings = AppService.getAppSetting();
    $scope.categoryData = [];
    $scope.openCategory = function($categoryId) {
        angular.forEach($scope.categoryData, function(category, key) {
            if (category.id === $categoryId) {
                window.localStorage.setItem("categoryName", category.name);
                window.localStorage.setItem("categoryItem", JSON.stringify(category));
                if(settings.category_display === 'subcategories'){
                    if(category.children.length > 0){
                        $state.go('tab.categories', {categoryId: category.id});
                    }else{
                        $state.go('tab.category', {categoryId: category.id});
                    }
                }else{
                    $state.go('tab.category', {categoryId: category.id});
                }
                return true;
            }
        });
    };

    $scope.getCategoryListData = function() {
        ListCategoryService.getListCategory().then(function(listCategory) {
            $scope.categoryData = listCategory;
        });
    };

    if($stateParams.categoryId == 'all'){
        //call function init data, use beforenter event if you want to clear the cache data when back
        $scope.getCategoryListData();
        $scope.pageTitle = $rootScope.appLanguage.CATEGORIES_TEXT;
    }else{
        var _category = JSON.parse(window.localStorage.getItem("categoryItem"));
        $scope.categoryData = _category.children;
        $scope.pageTitle = window.localStorage.getItem("categoryName");
    }


    $scope.doRefresh = function() {
        ListCategoryService.getListCategory().then(function(listCategory) {
            $scope.categoryData = listCategory;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

});