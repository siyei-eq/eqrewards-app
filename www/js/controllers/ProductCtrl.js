Ctrl.controller('ProductCtrl', function($scope, $ionicModal, $sce, $timeout, $ionicPopup, $rootScope,
                                       $ionicSlideBoxDelegate, appConfig, CartService, WishlistService,
                                       ReviewsService, UserService, AnimationService) {
    $scope.product = JSON.parse(window.localStorage.getItem("product"));
    angular.forEach($scope.product.attributes, function(value, key) {
        if(value.variation != true) {
            delete $scope.product.attributes[key];
            $scope.product.attributes.length -= 1;
        }
    });

    if( $scope.product.price >0  ) {
        $scope.priceChoice = 'total';
    } else {
        $scope.priceChoice = 'partial';
    }

    $scope.productQuantity  = 100;
    $scope.productQuantity  = parseInt($scope.product.quantity);
    if($scope.product.manage_stock) {
        $scope.productQuantity = $scope.product.stock_quantity;
        if($scope.product.in_stock && $scope.product.stock_quantity === 0) {
            $scope.productQuantity = 100;
        }
    }
    $scope.productFormData = {};
    $scope.productFormData.attributes = [];
    $scope.productFormData.quantity = "1";
    $scope.productFormData.info = $scope.product;
    $scope.reviewSubmitFormData = {};
    $scope.reviewSubmitFormData.reviewRating = 0;
    $scope.variationPrice = "";
    $scope.reviews = [];
    $scope.isInWishlist = false;//WishlistService.checkProductInWishlist($scope.product.id);
    angular.forEach($scope.product.attributes, function(value, key) {
        $scope.productFormData.attributes[key] = "";
    });
    $scope.getProductQuanity = function(productQuantity) {
        productQuantity = ( productQuantity == -1 ) ? 100 : productQuantity;
        return new Array(productQuantity);
    };
    $scope.productFormData.variation_id = -1;
    $scope.shareProduct = function() {
        var options = {
            message: $scope.product.name, // not supported on some apps (Facebook, Instagram)
            url: $scope.product.permalink,
            chooserTitle: $scope.product.name // Android only, you can override the default share sheet title
        };
        window.plugins.socialsharing.shareWithOptions(options, $scope.onSuccessShare);
    };

    $scope.addProductToWishlist = function() {
        //something to add product into cart
        var productId = $scope.product.id;
        var quantity = parseInt($scope.productFormData.quantity);
        var info = $scope.productFormData.info;
        if(!$scope.isInWishlist) {
            WishlistService.addProductToWishlist(productId, quantity, info);
            AnimationService.moveProductToCartAnimation();
            $timeout(function() {
                AnimationService.actionByClass('wishlist', 'swing');
                $rootScope.wishlistQuantity = WishlistService.getWishlistQuantity();
            }, 1000);
            $scope.isInWishlist = true;
            return true;
        }
        else {
            WishlistService.removeProductFromWishlist(productId);
            $scope.isInWishlist = false;
            return true;
        }

    };

    $scope.addProductToCart = function() {
        if( $scope.priceChoice === null ){
            $ionicPopup.alert({
                title: $rootScope.appLanguage.MESSAGE_TEXT,
                template: 'Por favor seleccione una opción de canje'
            });
            return false;
        }
        //something to add product into cart
        var productId = $scope.product.id;
        var quantity = parseInt($scope.productFormData.quantity);
        var info = angular.copy($scope.productFormData.info);
        info.price_type = $scope.priceChoice;

        CartService.addProductToCart(productId, quantity, info);
        AnimationService.moveProductToCartAnimation();
        $timeout(function() {
            AnimationService.actionByClass('shoppingcart', 'swing');
            $rootScope.cartQuantity = CartService.getCartQuantity();
        }, 1000);

        return true;
    };

    $scope.onSuccessShare = function() {
        //something after success share
    };

    // Modal info
    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/product-info.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.mInfo = modal;
    });

    $scope.closeModalInfo = function() {
        $scope.mInfo.hide();
    };
    $scope.openModalInfo = function() {
        $scope.mInfo.show();
    };
    // End Modal info

    // Start Modal Add Review
    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/add-review.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.mAddReview = modal;
    });

    $scope.closeModalCloseReview = function() {
        $scope.mAddReview.hide();
    };
    $scope.openModalAddReview = function() {
        $scope.mAddReview.show();
    };
    // End Modal Add Review

    $scope.loadReview = function(){
        if($scope.reviews.length <= 0){
            ReviewsService.getProductReviews($scope.product.id).then(function(reviews) {
                $scope.reviews = reviews;
            });
        }
    };

    // render rating
    $scope.renderRatting = function($rate){
        $rate = $rate * 20;
        return $sce.trustAsHtml('<div class="rate"><span style="width: '+$rate+'%;"></span></div>');
    };

    $scope.ratingsObject = {
        iconOn: 'ion-ios-star',
        iconOff: 'ion-ios-star-outline',
        iconOnColor: '#eab12a',
        iconOffColor: '#eab12a',
        rating: 0,
        minRating: 0,
        readOnly:false,
        callback: function(rating) {
            // $scope.ratingsCallback(rating);
            $scope.reviewSubmitFormData.reviewRating = rating;
        }
    };

    $scope.submitProductReview = function(reviewSubmitForm) {
        if (!reviewSubmitForm.$valid) {
            return false;
        }
        var productId = $scope.product.id;
        var userId = UserService.getUserId();
        var firstName = $scope.reviewSubmitFormData.first_name;
        var lastName = $scope.reviewSubmitFormData.last_name;
        var comment = $scope.reviewSubmitFormData.comment;
        var rating = $scope.reviewSubmitFormData.reviewRating;
        var email = $scope.reviewSubmitFormData.user_email;
        if(rating === 0){
            $ionicPopup.alert({
                title: $rootScope.appLanguage.MESSAGE_TEXT,
                template: 'Please select a rating'
            });
            return false;
        }
        ReviewsService.submitProductReview(productId, userId, firstName, lastName, email, comment, rating);
        $ionicPopup.alert({
            title: $rootScope.appLanguage.MESSAGE_TEXT,
            template: 'Your comment is awaiting approval'
        });
        $scope.closeModalCloseReview();
    };

    $scope.isLoggedIn = function() {
        return !!UserService.isLoggedIn();
    };

    $scope.updateProductAttribute = function($index, $value) {
        $scope.productFormData.attributes[$index] = $value;
        $scope.productFormData.variation_id = -1;
        angular.forEach($scope.product.variations, function(variation, variationKey) {
            var count = 0;
            angular.forEach($scope.productFormData.attributes, function(value, key) {
                for(var i = 0; i < variation.attributes.length; i++) {
                    if(!Array.isArray(variation.attributes[i].option)) {
                        if(value == variation.attributes[i].option) {
                            count++;
                        }
                    } else {
                        if(variation.attributes[i].option.indexOf(value) != -1) {
                            count++;
                        }
                    }

                }
            });
            if (count === $scope.productFormData.attributes.length) {
                $scope.productFormData.variation_id = variation.id; //update new product variation id
                $scope.variationPrice = variation.price;
                $scope.productFormData.info = variation;
                $scope.productFormData.info.name = $scope.product.name;
                $scope.productFormData.info.product_id = $scope.product.id;
                //update product quantity by variation quantity
                if(variation.manage_stock) {
                    $scope.productQuantity = variation.stock_quantity;
                }
                $scope.product.in_stock = variation.in_stock;
                $scope.productFormData.quantity = "1";
                //check variation image in product image
                var flagVariationImage = false;
                if(variation.image) {
                    angular.forEach($scope.product.images, function(image, key) {
                        if (image.id === variation.image[0].id) {
                            flagVariationImage = true;
                            $ionicSlideBoxDelegate.slide(key);
                        }
                    });
                    if(!flagVariationImage) {
                        $scope.product.images.push(variation.image[0]);
                        $ionicSlideBoxDelegate.update();
                        $timeout(function(){$ionicSlideBoxDelegate.slide($scope.product.images.length - 1);}, 100);
                    }
                }
                if(variation.images) {
                    var flagAddNewImage = false;
                    angular.forEach(variation.images, function(variationImage, variationKey) {
                        flagVariationImage = false;
                        angular.forEach($scope.product.images, function(image, key) {
                            if (image.id === variationImage.id) {
                                flagVariationImage = true;
                                $ionicSlideBoxDelegate.slide(key);
                            }
                        });
                        if(!flagVariationImage) {
                            $scope.product.images.push(variationImage);
                            $ionicSlideBoxDelegate.update();
                            flagAddNewImage = true;
                        }
                    });
                    if(flagAddNewImage) {
                        $timeout(function(){$ionicSlideBoxDelegate.slide($scope.product.images.length - 1);}, 100);
                    }
                }

            }
        });

    };

    $scope.$on("$ionicView.beforeEnter", function(event, data) {
        //something before enter the view
    });

    $scope.optionSelected = function(opt){
        $scope.priceChoice = opt;
        console.debug($scope.priceChoice);
    };

});