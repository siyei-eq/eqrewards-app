Srv.factory('CartService', function($ionicLoading, $rootScope, AppService, $http, appConfig, appValue) {

    var cartInfo = {};
    cartInfo.products = [];
    if(window.localStorage.getItem("cartInfo") && window.localStorage.getItem("cartInfo") !== "undefined") {
        cartInfo = JSON.parse(window.localStorage.getItem("cartInfo"));
    }
    return {
        clearCart: function() {
            cartInfo = {};
            cartInfo.products = [];
            this.updateCartInfo();
            $rootScope.cartQuantity = this.getCartQuantity();
        },
        addProductToCart: function($productId, $quantity, $info) {
            var quantity = $quantity;
            var isNew = true;
            angular.forEach(cartInfo.products, function(product, key) {
                if (product[0] === $productId) {
                    if( product[2].price_type === $info.price_type ) {
                        quantity = quantity + product[1];
                        cartInfo.products[key][1] = quantity;
                        isNew = false;
                    }
                }
            });
            if(isNew === true) {
                var product = [$productId, $quantity, $info];
                cartInfo.products.push(product);
            }
            this.updateCartInfo();

            return true;
        },
        updateCart: function($productId, $price_type, $quantity) {
            angular.forEach(cartInfo.products, function(product, key) {
                if ( product[0] === $productId && $price_type == product[2].price_type ){
                    cartInfo.products[key][1] = $quantity;
                    //isNew = false;
                }
            });

            this.updateCartInfo();
            return true;
        },
        removeProductFromCart: function($productId, $price_type) {
            angular.forEach(cartInfo.products, function(product, key) {
                if ( $price_type === product[2].price_type && product[0] === $productId ) {
                    cartInfo.products.splice(key, 1);
                }
            });
            this.updateCartInfo();
            $rootScope.cartQuantity = this.getCartQuantity();
            return true;
        },
        getCartInfo: function() {
            return cartInfo;
        },
        getCartQuantity: function() {
            var total = 0;
            angular.forEach(cartInfo.products, function(product, key) {
                total = total + product[1];
            });
            return total;
        },
        getCartTotal: function() {
            var total = 0;
            angular.forEach(cartInfo.products, function(product, key) {
                total = total + product[2].price * product[1];
            });
            return total;
        },
        getCartTotalPoints: function() {
            var total = 0;
            angular.forEach(cartInfo.products, function(product, key) {
                if( product[2].price_type !== "partial" ) {
                    total = total + product[2].price * product[1];
                } else {
                    total = total + product[2].partial_points * product[1];
                }
            });
            return total;
        },
        getCartTotalMoney: function() {
            var total = 0;
            angular.forEach(cartInfo.products, function(product, key) {
                if( product[2].price_type === "partial" ) {
                    total = total + product[2].plus_amount * product[1];
                }
            });
            return total;
        },
        updateCartInfo: function() {
            window.localStorage.setItem("cartInfo", JSON.stringify(cartInfo));
        },
        updateCartQuantity: function(productOutOfStock){
            var cartInfo = this.getCartInfo();
            angular.forEach(cartInfo.products, function(product, key) {
                angular.forEach(productOutOfStock, function(v, k){
                    if( v.product == product[0] ){
                        //product[1] = parseInt(v.quantity);
                        product[2].quantity = parseInt(v.quantity);
                    }
                });
            });
            window.localStorage.setItem("cartInfo", JSON.stringify(cartInfo));
        }
    };
});