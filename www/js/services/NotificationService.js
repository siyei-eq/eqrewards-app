Srv.factory('NotificationService', function($state, $ionicPopup, $rootScope, UserService) {
    return {
        saveDeviceToken: function(token) {
            window.localStorage.setItem("deviceToken", token);
        },
        getDeviceToken: function() {
            return window.localStorage.getItem("deviceToken");
        },
        doNotification: function(notification) {
            if(notification.payload.type === 'text') {
                this.textNotification(notification);
            }else if(notification.payload.type === 'order'){
                this.orderNotification(notification);
            }
        },
        textNotification: function(notification){
            window.localStorage.setItem("appNotificationPayload", JSON.stringify(notification.payload));
            if(!notification._raw.additionalData.foreground) {
                $state.go('tab.notification');
            }
            else {
                var confirmPopup = $ionicPopup.confirm({
                    title: $rootScope.appLanguage.NOTIFICATION_TEXT,
                    template: 'You have a new notification - go to it?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        $state.go('tab.notification');
                    }
                });
            }
        },
        orderNotification: function(notification){
            if(!notification._raw.additionalData.foreground) {
                $state.go('tab.orders');
            }
            else {
                var confirmPopup = $ionicPopup.confirm({
                    title: $rootScope.appLanguage.NOTIFICATION_TEXT,
                    template: 'You have a new notification - go to it?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        $state.go('tab.orders');
                    }
                });
            }
        }
    };
});