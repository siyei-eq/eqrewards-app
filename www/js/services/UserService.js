Srv.factory('UserService', function($ionicLoading, $http, $ionicPopup, $rootScope, appConfig, appValue, AppService) {
    var loginStatus = false;
    return {
        isLoggedIn: function() {
            if(window.localStorage.getItem("is_login") !== null && window.localStorage.getItem("is_login") === 'true'){
                return true;
            }
            else {
                return false;
            }
        },

        logout: function() {
            //window.localStorage.setItem("is_login", false);
            window.localStorage.setItem("userInfo", '');
            window.localStorage.removeItem('id_token');
            window.localStorage.removeItem('profile');
            window.localStorage.removeItem('last_check');
        },

        login: function($loginData) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'users/login',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: $loginData
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        window.localStorage.setItem("last_email_login", $loginData.user_login);
                        window.localStorage.setItem("is_login", true);
                        window.localStorage.setItem("userInfo", JSON.stringify(response.data.data));
                        loginStatus = true;
                        return loginStatus;
                    }
                    else {
                        /*$ionicPopup.alert({
                            title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: response.data.message
                        });*/
                        AppService.alert(response.data.message, $rootScope.appLanguage.MESSAGE_TEXT);
                        return loginStatus;
                    }
                }, function error(response){
                    $ionicLoading.hide();
                    /*$ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });*/
                    var message = $rootScope.appLanguage.NETWORK_OFFLINE_TEXT;
                    if( response.data.hasOwnProperty('message') ){
                        message = response.data.message;
                    }
                    AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);
                    return loginStatus;
                });
        },

        register: function($registerData) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: "POST",
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'users/register',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: $registerData
            }).then(function(response) {
                $ionicLoading.hide();
                // handle success things
                if(response.data.status === appValue.API_SUCCESS){
                    window.localStorage.setItem("is_login", true);
                    window.localStorage.setItem("userInfo", JSON.stringify(response.data.data));
                    loginStatus = true;
                    return loginStatus;
                }
                else {
                    /*$ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: response.data.message
                    });*/
                    AppService.alert(response.data.message, $rootScope.appLanguage.MESSAGE_TEXT);
                    return loginStatus;
                }
            }, function error(response){
                $ionicLoading.hide();
                var message = $rootScope.appLanguage.NETWORK_OFFLINE_TEXT;
                if( response.data.hasOwnProperty('message') ){
                    message = response.data.message;
                }
                /*$ionicPopup.alert({
                    title: $rootScope.appLanguage.MESSAGE_TEXT,
                    template: message
                });*/
                AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);
                return loginStatus;
            });
        },

        forgotpass: function($forgotpassData) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'users/recovery',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: $forgotpassData
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    /*$ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: $rootScope.appLanguage.FORGOT_SUCCESS_MESSAGE
                    });*/
                    AppService.alert($rootScope.appLanguage.FORGOT_SUCCESS_MESSAGE, $rootScope.appLanguage.MESSAGE_TEXT);
                    return true;
                },function error(response){
                    $ionicLoading.hide();
                    /*$ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });*/
                    var message = $rootScope.appLanguage.NETWORK_OFFLINE_TEXT;
                    if( response.data.hasOwnProperty('message') ){
                        message = response.data.message;
                    }
                    AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);
                    return true;
                });
        },

        editAccount: function($editAccountFormData) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'users/update',
                    headers: { 'Authorization': this.getAccessToken(), 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: $editAccountFormData
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    var message = undefined;
                    if( response.data.hasOwnProperty('message') ){
                        message = response.data.message;
                    }
                    if(response.data.status === appValue.API_SUCCESS){
                        /*$ionicPopup.alert({
                            title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: ( message === undefined ? 'Se han actualizado los datos' : message )
                        });*/

                        message = (message === undefined) ? 'Se han actualizado los datos' : message;

                        AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);

                        return true;
                    }
                    else {
                        /*$ionicPopup.alert({
                            title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: ( message === undefined ?
                                'Ocurrió un error desconocido al actualizar los datos. Por favor intentelo más tarde' :
                                message
                            )
                        });*/
                        messsage =  ( message === undefined ?
                            'Ocurrió un error desconocido al actualizar los datos. Por favor intentelo más tarde' :
                                message);

                        AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);
                        return false;
                    }
                },function error(response){
                    $ionicLoading.hide();
                    var message = $rootScope.appLanguage.NETWORK_OFFLINE_TEXT;
                    if( response.data.hasOwnProperty('message') ){
                        message = response.data.message;
                    }
                    /*$ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: message
                    });*/
                    AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);
                    return false;
                });
        },

        getUser: function() {
            if(window.localStorage.getItem("userInfo")){
                var userInfo = JSON.parse(window.localStorage.getItem("userInfo"));
                userInfo.points = userInfo.points == null ? 0 : userInfo.points;
                if(userInfo.billing && userInfo.billing.length === 0) {
                    userInfo.billing = {"first_name":"","last_name":"","company":"","address_1":"","address_2":"","city":"","state":"","postcode":"","country":"","email":"","phone":""};
                }
                if(userInfo.shipping && userInfo.shipping.length === 0) {
                    userInfo.shipping = {"first_name":"","last_name":"","company":"","address_1":"","address_2":"","city":"","state":"","postcode":"","country":"","email":"","phone":""};
                }
                return  userInfo;
            }
            else {
                return {
                    "billing":{"first_name":"","last_name":"","company":"","address_1":"","address_2":"","city":"","state":"","postcode":"","country":"","email":"","phone":""},
                    "shipping":{"first_name":"","last_name":"","company":"","address_1":"","address_2":"","city":"","state":"","postcode":"","country":"","email":"","phone":""},
                    "email":"","first_name":"","last_name":"","username":""
                };
            }
        },

        updateUser: function($user) {
            window.localStorage.setItem("userInfo", JSON.stringify($user));
        },

        getUserId: function(){
            var user = this.getUser();
            if(user) {
                return user.id;
            }
            else {
                return "";
            }
        },

        getAccessToken: function(){
            var user = this.getUser();
            if(user) {
                return user.access_token;
            }
            else {
                return "";
            }
        },

        getPoints: function(){
            var user = this.getUser();
            if( user ) {
                return user.points == null ? 0 : user.points;
            }
            else {
                return 0;
            }
        },

        updateUserData: function(showSpinner) {
            showSpinner = ( typeof showSpinner === "undefined" );
            if( showSpinner ) {
                $ionicLoading.show({
                    template: '<ion-spinner></ion-spinner>'
                });
            }
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'users',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization' : this.getAccessToken()
                }
            }).then(function(response) {
                if( showSpinner ) {
                    $ionicLoading.hide();
                }
                if(response.data.status === appValue.API_SUCCESS){
                    var userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
                    userInfo.points = response.data.data.points;
                    window.localStorage.setItem("userInfo", JSON.stringify(userInfo));
                }
            }, function error(response){

            });
        }

    };
});