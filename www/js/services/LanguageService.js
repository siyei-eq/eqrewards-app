Srv.factory('LanguageService', function($state, $ionicPopup, $rootScope) {
    return {
        saveLanguage: function(language) {
            window.localStorage.setItem("appLanguage", language);
        },
        getLanguage: function() {
            if(window.localStorage.getItem("appLanguage")) {
                return window.localStorage.getItem("appLanguage");
            }
            else {
                return 'es';
            }

        }
    };
});