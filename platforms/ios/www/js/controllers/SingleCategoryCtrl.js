Ctrl.controller('SingleCategoryCtrl', function($scope, $stateParams, $state, ProductService) {
    $scope.pageCategory = 1;
    $scope.categoryData = [];
    $scope.canLoadMoreCategoryData = true;
    $scope.categoryName = window.localStorage.getItem("categoryName");
    $scope.openProduct = function($productId) {
        angular.forEach($scope.categoryData, function(product, key) {
            if (product.id === $productId) {
                window.localStorage.setItem("product", JSON.stringify(product));
                $state.go('tab.product', {productId: $productId});
                return true;
            }
        });
    };
    $scope.getCategoryData = function($page) {
        ProductService.getProducts($page,'&type=category&param=' + $stateParams.categoryId)
            .then(function(listProduct) {
                if(!listProduct){
                    $scope.canLoadMoreCategoryData = false;
                    return false;
                }

                if (listProduct.length > 0) {
                    angular.forEach(listProduct, function(product, key) {
                        $scope.categoryData.push(product);
                    });
                    $scope.pageCategory = $page + 1;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.canLoadMoreCategoryData = true;
                }
                else {
                    $scope.canLoadMoreCategoryData = false;
                }
            });
    };
    $scope.loadMoreCategoryData = function() {
        $scope.getCategoryData($scope.pageCategory);
    };
    $scope.refreshCategoryData = function() {
        $scope.pageCategory = 1;
        $scope.categoryData = [];
        $scope.canLoadMoreCategoryData = false;
        $scope.getCategoryData(1);
    };
});