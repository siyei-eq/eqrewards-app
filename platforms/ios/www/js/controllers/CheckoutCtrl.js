Ctrl.controller('CheckoutCtrl', function($scope, $state, UserService) {
    $scope.loginData = {};
    $scope.doLogin = function(loginForm) {
        if (!loginForm.$valid) {
            return false;
        }
        UserService.login($scope.loginData).then(function(response) {
            if(response === true){
                $scope.nextStep();
            }
        });
    };
    $scope.nextStep = function(){
        $state.go('tab.checkout-billing');
    };
    $scope.$on("$ionicView.beforeEnter", function(event, data) {

    });
});