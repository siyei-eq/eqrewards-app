Ctrl.controller('SingleBlogCtrl', function($scope, $stateParams, $rootScope, appConfig,
                                          $state, BlogService, $sce, UserService, $ionicModal, $ionicPopup) {
    $scope.blogDetail = {};
    $scope.pageTitle = $rootScope.appLanguage.BLOG_TEXT;
    $scope.commentData = {};

    $scope.getBlogDetail = function($blogId) {
        BlogService.getSingleBlog($blogId).then(function(listBlog) {
            $scope.blogDetail = listBlog;
            $scope.pageTitle = $scope.blogDetail.title;
        });
    };
    $scope.unescapeHtml = function(string) {
        var e = document.createElement('div');
        e.innerHTML = $sce.trustAsHtml(string);
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    };

    // Start Modal Add Review
    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/add-comment.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.mAddComment = modal;
    });

    $scope.closeAddComment = function() {
        $scope.mAddComment.hide();
    };
    $scope.openAddComment = function() {
        $scope.mAddComment.show();
    };
    $scope.submitComment = function(form) {
        if (!form.$valid) {
            return false;
        }

        var blogId = $scope.blogDetail.id;
        var userId = UserService.getUserId();
        var name = $scope.commentData.name;
        var comment = $scope.commentData.comment;
        var email = $scope.commentData.user_email;

        BlogService.submitComment(blogId, userId, name, email, comment);

        $scope.closeAddComment();
    };
    // End Modal Add Review

    $scope.isLoggedIn = function() {
        return UserService.isLoggedIn();
    };

    $blogId = $stateParams.blogId;
    $scope.getBlogDetail($blogId);
});