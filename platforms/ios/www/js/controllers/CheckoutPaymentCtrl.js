Ctrl.controller('CheckoutPaymentCtrl', function($scope, $state, $ionicHistory, $ionicPopup, $rootScope, OrderService,
                                               UserService, AppService, CartService, appConfig, $injector, appValue) {
    $scope.orderInfo = {};
    $scope.orderInfo.payment = '';
    $scope.orderInfo.shipping = '';
    $scope.cartInfo = CartService.getCartInfo();
    $scope.listPayment = [];
    $scope.userInfo = UserService.getUser();
    //$scope.orderInfo.coupon = "";
    try {
        $scope.orderShipping = JSON.parse(OrderService.getOrderInfo().shipping);
    } catch(err){
        $scope.orderShipping = $scope.userInfo.shipping;
    }

    $scope.cartPointsTotal = CartService.getCartTotalPoints();
    $scope.cartMoneyTotal = CartService.getCartTotalMoney();

    $scope.createOrder = function() {
        var line_items = [];
        angular.forEach($scope.cartInfo.products, function(product, key) {
            if(product[2].product_id !== undefined){
                line_items.push({
                    product_id: product[2].product_id,
                    price_type: product[2].price_type,
                    quantity: product[1],
                    price : product[2].price,
                    partial_points : product[2].partial_points,
                    plus_amount : product[2].plus_amount
                });
            }
            else {
                line_items.push({
                    product_id: product[0],
                    price_type: product[2].price_type,
                    quantity: product[1],
                    price : product[2].price,
                    partial_points : product[2].partial_points,
                    plus_amount : product[2].plus_amount
                });
            }
        });
        // Add Device Token
        var device_token = window.localStorage.getItem("deviceToken");
        if(typeof device_token !== 'undefined'){
            OrderService.updateDeviceToken(device_token);
        }

        OrderService.updateOrderInfoLineItems(line_items);
        $scope.orderShipping.total_points = $scope.cartPointsTotal;
        $scope.orderShipping.total_money = $scope.cartMoneyTotal;
        OrderService.updateOrderInfoShipping($scope.orderShipping);
        OrderService.createOrder().then(function(response) {
            var message = "Un error inespeado ha ocurrido con su orden. Por favor intente más tarde.";
            if(response === true) {
                CartService.clearCart();
                OrderService.clearOrderInfo();
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('tab.checkout-success');
            } else if( response !== false ) {

                if( response.hasOwnProperty('errno') ) {
                    if (response.errno === AppService.getAppSetting().out_of_stock) {
                        $scope.$emit('updateCartQuantity', response.data);
                        //-1 CheckoutBilling -2 CartCtrl
                        $ionicHistory.goBack(-2);
                    }
                }
                if( response.hasOwnProperty('message') && response.message != '' ){
                    message = response.message;
                }
                $ionicPopup.alert({
                    title: $rootScope.appLanguage.MESSAGE_TEXT,
                    template: message
                });

            } else {
                $ionicPopup.alert({
                    title: $rootScope.appLanguage.MESSAGE_TEXT,
                    template: message//"Un error inespeado ha ocurrido con su orden. Por favor intente más tarde."
                });
            }
        });
    };
});