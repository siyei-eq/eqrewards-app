Ctrl.controller('BlogCategoryCtrl', function($scope, $state, $stateParams, $rootScope, BlogService) {
    $scope.page = 1;
    $scope.canLoadMoreData = false;
    $scope.categoryId = $stateParams.categoryId;
    $scope.blogData = [];

    $scope.getBlogListData = function(categoryId, $page, $perpage) {
        $page = $page ? $page : 1;
        $perpage = $perpage ? $perpage : 10;

        BlogService.getListBlogByCategory(categoryId, $page, $perpage).then(function(listBlog) {
            if(!listBlog){
                $scope.canLoadMoreData = false;
                return false;
            }

            if (listBlog.length > 0) {
                angular.forEach(listBlog, function(blog) {
                    $scope.blogData.push(blog);
                });
                $scope.page = $page + 1;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.canLoadMoreData = true;
            }
            else {
                $scope.canLoadMoreData = false;
            }
        });
    };

    $scope.refreshData = function() {
        $scope.page = 1;
        $scope.canLoadMoreData = false;
        $scope.blogData = [];
        $scope.getBlogListData($scope.categoryId, $scope.page);
    };

    $scope.loadMoreData = function() {
        $scope.getBlogListData($scope.categoryId, $scope.page);
    };

    $scope.openBlog = function($blogId) {
        $state.go('tab.blog-single', {blogId: $blogId})
    };

    $scope.removeReadmore = function(string) {
        var trunks = string.split('<a');
        trunks.pop();
        return trunks.join('<a');
    };

    $scope.getBlogListData($scope.categoryId);

    var $title = window.localStorage.getItem("currentBlogCategory");
    $scope.pageTitle = $title ? $title : $rootScope.appLanguage.BLOG_TEXT;
});