Ctrl.controller('WishListCtrl', function($scope, $stateParams, $state, WishlistService) {
    $scope.wishlistInfo = WishlistService.getWishlistInfo();
    $scope.products = $scope.wishlistInfo.products;
    $scope.openProduct = function($productId) {
        angular.forEach($scope.products, function(product, key) {
            if (product[2].id === $productId) {
                window.localStorage.setItem("product", JSON.stringify(product[2]));
                $state.go('tab.product', {productId: $productId});
                return true;
            }
        });
    };
});