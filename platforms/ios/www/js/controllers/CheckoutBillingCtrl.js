Ctrl.controller('CheckoutBillingCtrl', function($scope, $state, $ionicPopup, $rootScope, AppService, UserService, OrderService) {
    $scope.userInfo = UserService.getUser();

    //check billing obj
    if( !$scope.userInfo.hasOwnProperty('billing') ){
        $scope.userInfo.billing = {
            name : '',
            phone : '',
            address : '',
            city : ''
        };
    }

    if( $scope.userInfo.billing.name === '' ){
        $scope.userInfo.billing.name = $scope.userInfo.name;
    }

    if( $scope.userInfo.billing.phone === '' ){
        $scope.userInfo.billing.phone = $scope.userInfo.phone;
    }

    if( $scope.userInfo.billing.address === '' ){
        $scope.userInfo.billing.address = $scope.userInfo.address;
    }

    if( $scope.userInfo.billing.city === '' ){
        $scope.userInfo.billing.city = $scope.userInfo.city;
    }

    /*$scope.listCountry = AppService.getListCountry().then(function(listCountry) {
        $scope.listCountry = listCountry;
    });*/

    $scope.listState = {};

    $scope.changeCountry = function() {
        //remove state default
        $scope.userInfo.billing.state = '';
    };

    $scope.checkCountryHasState = function() {
        var result = false;
        angular.forEach($scope.listCountry, function(country, key) {
            if (country.id === $scope.userInfo.billing.country) {
                if(country.state && country.state.length !== 0) {
                    $scope.listState = country.state;
                    result = true;
                }
            }
        });
        return result;
    };

    $scope.nextStep = function() {
        OrderService.updateOrderInfoBilling($scope.userInfo.billing);
        if( OrderService.validateOrderInfoBilling() ) {
            $scope.userInfo.shipping = $scope.userInfo.billing;
            OrderService.updateOrderInfoShipping($scope.userInfo.shipping);
            UserService.updateUser($scope.userInfo);
            //$state.go('tab.checkout-note');
            $state.go('tab.checkout-payment');
        }
        else {
            $ionicPopup.alert({
                title: $rootScope.appLanguage.MESSAGE_TEXT,
                template: $rootScope.appLanguage.REQUIRED_ERROR_TEXT
            });
        }
    };
});