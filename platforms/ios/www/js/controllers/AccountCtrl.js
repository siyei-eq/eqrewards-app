Ctrl.controller('AccountCtrl', function($scope, $ionicPopup, $rootScope, UserService, appConfig, appValue,
                                        $cordovaImagePicker, $cordovaFileTransfer) {
    $scope.listCountry = [];
        $scope.userInfo = UserService.getUser();
    $scope.userInfo.password = "";
    $scope.userInfo.newpassword = "";
    $scope.userInfo.confirmnewpassword = "";
    $scope.updateAvatar = false;

    $scope.doEditAccount = function(editAccountForm) {
        if (!editAccountForm.$valid) {
            return false;
        }else if( $scope.userInfo.password !== ''){
            if( $scope.userInfo.newpassword === '' ){
                editAccountForm.new_password.$error.required = true;
                return false;
            }else if($scope.userInfo.newpassword !== $scope.userInfo.confirmnewpassword){
                editAccountForm.confirm_new_password.$error.required = true;
                return false;
            }
        }

        var editAccountFormData = {};
        editAccountFormData.name = $scope.userInfo.name;
        editAccountFormData.email = $scope.userInfo.email;
        editAccountFormData.dni = $scope.userInfo.dni;
        editAccountFormData.phone = $scope.userInfo.phone;
        editAccountFormData.user_pass = $scope.userInfo.password;
        editAccountFormData.user_new_password = $scope.userInfo.newpassword;
        editAccountFormData.user_confirmation = $scope.userInfo.confirmnewpassword;
        editAccountFormData.user_id = $scope.userInfo.id;
        UserService.editAccount(editAccountFormData).then(function(response) {
            if(response === true){
                UserService.updateUser($scope.userInfo);
            }
        });

        if( true || $scope.updateAvatar ){

            var filePath = $scope.userInfo.avatar;
            var fileName = filePath.split("/").pop();
            var server = appConfig.DOMAIN_URL + appValue.API_URL + 'users/avatar';
            var options = {
                headers: {
                    fileKey: "archivo",
                    fileName: fileName,
                    chunkedMode: true,
                    mimeType: "image/jpg",
                    Authorization: UserService.getAccessToken()
                }
            };

            $scope.updateAvatar = false;
            $cordovaFileTransfer.upload(server,
                filePath, options
            ).then(function(result) {
                try {
                    var res = JSON.parse(result.response);
                    if (res.status) {
                        $scope.userInfo.avatar = res.data.link;
                    }
                } catch(err){

                }
            }, function(err) {
                console.debug(err);
            }, function (progress) {
                console.debug(progress);
            });
        }
    };

    $scope.getPicture = function () {
        console.log("getPicture");
        cordova.plugins.diagnostic.requestCameraAuthorization(function(status){
            $cordovaImagePicker.getPictures({
                maximumImagesCount: 1,
                width: 800,
                height: 800,
                quality: 80
            }).then(function (results) {
                for (var i = 0; i < results.length; i++) {
                    $scope.userInfo.avatar = results[i];
                    $scope.updateAvatar = true;
                }
            }, function(error) {
            });

        }, function(error){
            console.error("The following error occurred: "+error);
        });

    }
});