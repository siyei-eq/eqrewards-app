Ctrl.controller('ContactUsCtrl', function($scope, $ionicPopup, $state, $ionicModal, appConfig, AppService, NgMap, $cordovaEmailComposer) {
    $scope.appSettings = AppService.getAppSetting();
    //$scope.info_position = $scope.appSettings.contact_map_lat + ',' + $scope.appSettings.contact_map_lng;
    $scope.shops = AppService.getShopsList();

    $scope.zoomLevel = 13;

    $scope.info_position = $scope.shops[0].latitude + "," +     $scope.shops[0].longitude;

    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/contact.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.mContact = modal;
    });

    $scope.sendMail = function(mail) {
        if( mail == undefined ||  mail == null ){
            mail = $scope.appSettings.contact_email;
        }


        $cordovaEmailComposer.isAvailable().then(function() {
            console.log("ASDASD");
        }, function () {
            $ionicPopup.alert({
                title: $scope.$root.appLanguage.MESSAGE_TEXT,
                template: "No se ha podido acceder al correo del dispositivo"
            });
        });

        var email = {
            to: mail,
            subject: 'Contacto desde la App',
            body: '',
            isHtml: true
        };

        $cordovaEmailComposer.open(email).then(null, function () {
            console.log(email);
        });


    };

    $scope.callPhone = function(tel) {
        if( tel == undefined ||  tel == null || tel === "" ){
            tel = $scope.appSettings.contact_phone;
        }
        window.open('tel:' + tel, '_system');
    };

    $scope.openModalContact = function() {
        $scope.mContact.show();
    };

    $scope.closeModalContact = function() {
        $scope.mContact.hide();
    };

    var map;
    NgMap.getMap().then(function(evtMap){
        map = evtMap;
    });
    $scope.showWindow = function (evt, id) {
        $scope.selectedShop  = $scope.shops[id];

        $scope.selectedShop.html = $scope.selectedShop.description + '<br>' + $scope.selectedShop.address;
        map.showInfoWindow('marker-info', this);
    };
});