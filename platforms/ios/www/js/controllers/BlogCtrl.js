Ctrl.controller('BlogCtrl', function($scope, $state, $stateParams, $rootScope, BlogService, AppService) {
    var settings = AppService.getAppSetting();

    $scope.page = 1;
    $scope.canLoadMoreData = false;

    $scope.blogDisplay = settings['blog_display'];
    $scope.blogData = [];
    $scope.blogCategoryData = [];

    $scope.getBlogListData = function($page, $perpage) {
        $page = $page ? $page : 1;
        $perpage = $perpage ? $perpage : 10;

        BlogService.getListBlog($page, $perpage).then(function(listBlog) {
            if(!listBlog){
                $scope.canLoadMoreData = false;
                return false;
            }

            if (listBlog.length > 0) {
                angular.forEach(listBlog, function(blog) {
                    $scope.blogData.push(blog);
                });
                $scope.page = $page + 1;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.canLoadMoreData = true;
            }
            else {
                $scope.canLoadMoreData = false;
            }
        });
    };

    $scope.refreshData = function() {
        $scope.page = 1;
        $scope.canLoadMoreData = false;
        $scope.blogData = [];
        $scope.getBlogListData($scope.page);
    };

    $scope.loadMoreData = function() {
        $scope.getBlogListData($scope.page);
    };

    $scope.getBlogCategoryListData = function() {
        BlogService.getListBlogCategory().then(function(listCategory) {
            $scope.blogCategoryData = listCategory;
        });
    };

    $scope.openBlog = function($blogId) {
        $state.go('tab.blog-single', {blogId: $blogId})
    };

    $scope.openCategory = function($categoryId, $categoryName) {
        window.localStorage.setItem("currentBlogCategory", $categoryName);
        $state.go('tab.blog-category', {categoryId: $categoryId})
    };

    if ($scope.blogDisplay == 'posts') {
        $scope.getBlogListData();
        $scope.pageTitle = $rootScope.appLanguage.BLOG_TEXT;
    } else {
        $scope.getBlogCategoryListData();
        $scope.pageTitle = $rootScope.appLanguage.BLOG_CATEGORY_TEXT;
    }
});