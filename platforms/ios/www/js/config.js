angular.module('starter')
	//config param of App
	.constant('appConfig', {

			DOMAIN_URL: 'http://rewards.eq.com.py',
			ADMIN_EMAIL: 'cjvargas@eq.com.py',

			CLIENT_ID_AUTH0: 'H62fCSFGxrbKTRwArIJRdRjuhFnscgNo',
			DOMAIN_AUTH0: 'elquinchoit.auth0.com',

			ENABLE_FIRST_LOGIN: true,

			//fashion - foodia - eq
			ENABLE_THEME: 'eq',

			ENABLE_PUSH_PLUGIN: false,
			ENABLE_PAYPAL_PLUGIN: false,
			ENABLE_STRIPE_PLUGIN: false,
			ENABLE_RAZORPAY_PLUGIN: false,
			ENABLE_MOLLIE_PLUGIN: false,
			ENABLE_OMISE_PLUGIN: false
		}
	)


	//dont change this value if you dont know what it is
	.constant('appValue', {
		API_URL: '/api/',
		//API_URL: '/is-commerce/api/', //for worpdress and magento platform
		API_SUCCESS: true,
		API_FAIL: false
	})


	//list language
	.constant('listLanguage', [
			{code: 'en', text: 'English'},
			{code: 'es', text: 'Spanish'},
			{code: 'fr', text: 'French'}
		]
	);

(function () {
	new WOW().init();
});
