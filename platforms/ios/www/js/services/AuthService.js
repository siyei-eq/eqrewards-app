Srv.factory('AuthService', function($rootScope, angularAuth0, authManager, jwtHelper, $ionicPopup, $http, appValue, appConfig, $ionicLoading){
    var userProfile = JSON.parse(localStorage.getItem('profile')) || {};
    var pageScopeObject = "";
    return {

        loginWithGoogle : function ($pageScopeObject) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            pageScopeObject = $pageScopeObject;
            var thisObject = this;
            angularAuth0.login({
                connection: 'google-oauth2',
                responseType: 'token',
                popup: true
            }, function(error, authResult) {
                thisObject.callbackAuth0LoginWithGoogle(error, authResult);
            }, null);
        },

        loginWithFacebook : function ($pageScopeObject) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            pageScopeObject = $pageScopeObject;
            var thisObject = this;
            angularAuth0.login({
                connection: 'facebook',
                responseType: 'token',
                popup: true
            }, function(error, authResult){
                thisObject.callbackAuth0LoginWithFacebook(error, authResult);
            }, null);
        },

        authenticateAndGetProfile : function () {
            var result = angularAuth0.parseHash(window.location.hash);

            if (result && result.idToken) {
                callbackAuth0LoginWithGoogle(null, result);
            } else if (result && result.error) {
                callbackAuth0LoginWithGoogle(result.error);
            }
        },
        requestLoginWithSocial : function(profileData, platform) {
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'users?task=loginSocial&socialPlatform=' + platform,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: profileData
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        window.localStorage.setItem("is_login", true);
                        window.localStorage.setItem("userInfo", JSON.stringify(response.data.data));
                        pageScopeObject.closeModalLogin();
                    }
                    else {
                        $ionicPopup.alert({
                            title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: 'vdfvdfvdf'
                        });
                        return false;
                    }
                }, function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return false;
                });
        },
        callbackAuth0LoginWithFacebook : function (error, authResult) {
            if (error) {
                return $ionicPopup.alert({
                    title: 'Login failed!',
                    template: error
                });
            }
            var loginStatus = false;
            localStorage.setItem('id_token', authResult.idToken);
            authManager.authenticate();
            var thisObject = this;
            angularAuth0.getProfile(authResult.idToken, function (error, profileData) {
                if (error) {
                    return console.log(error);
                }
                localStorage.setItem('profile', JSON.stringify(profileData));
                thisObject.requestLoginWithSocial(profileData, 'facebook');
            });
        },
        callbackAuth0LoginWithGoogle : function (error, authResult) {
            if (error) {
                return $ionicPopup.alert({
                    title: 'Login failed!',
                    template: error
                });
            }
            var loginStatus = false;
            localStorage.setItem('id_token', authResult.idToken);
            authManager.authenticate();
            var thisObject = this;
            angularAuth0.getProfile(authResult.idToken, function (error, profileData) {
                if (error) {
                    return console.log(error);
                }
                localStorage.setItem('profile', JSON.stringify(profileData));
                thisObject.requestLoginWithSocial(profileData, 'google');
            });
        },

        checkAuthOnRefresh : function () {
            var token = localStorage.getItem('id_token');
            if (token) {
                if (!jwtHelper.isTokenExpired(token)) {
                    if (!$rootScope.isAuthenticated) {
                        authManager.authenticate();
                    }
                }
            }
        }
    };
});