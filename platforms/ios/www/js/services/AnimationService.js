Srv.factory('AnimationService', function($timeout, $ionicPosition) {
    return {
        action: function($id, $effect, $out) {
            if (typeof($out) === "undefined"){
                $out = 1000;
            }
            var element = angular.element(document.getElementById($id));
            element.addClass('animated '+$effect);
            $timeout(function(){
                element.removeClass('animated '+$effect);
            },$out);
        },
        actionByClass: function($class, $effect, $out) {
            if (typeof($out) === "undefined"){
                $out = 1000;
            }
            var element = angular.element(document.getElementsByClassName($class));
            element.addClass('animated '+$effect);
            $timeout(function(){
                element.removeClass('animated '+$effect);
            },$out);
        },
        moveProductToCartAnimation: function() {
            var parentElem 			= angular.element(document.getElementById("single-product")),
                parentPosition 			= $ionicPosition.position(parentElem),
                offsetTopCart 			= 10,
                offsetLeftCart			= parentPosition.width - 65,
                imgElem 				= angular.element(document.getElementsByClassName("single-main-img")),
                imgSrc 					= imgElem.prop("src"),
                imgClone 				= angular.element('<img class="itemaddedanimate" src="' + imgSrc + '"/>');
            imgClone.css({
                'position': 'absolute',
                'top': ($ionicPosition.position(imgElem).width/2 - $ionicPosition.position(imgClone).height/2) +'px',
                'left': (parentPosition.width/2 - 75) +'px',
                'opacity': 0.5
            });

            parentElem.append(imgClone);

            $timeout(function() {
                imgClone.css({
                    'width': '75px',
                    'top': offsetTopCart +'px',
                    'left': offsetLeftCart + 'px',
                    'opacity': 0.5
                });
            }, 300);

            $timeout(function () {
                imgClone.remove();
            }, 1000);
        }
    };
});