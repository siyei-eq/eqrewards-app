Srv.factory('ProductService', function( $http, $ionicPopup, $rootScope, $ionicLoading, appConfig, appValue, AppService) {
    var products = [];
    return {
        getProducts: function($page, $url, $isLoading) {
            if($isLoading){
                $ionicLoading.show({
                    template: '<ion-spinner></ion-spinner>'
                });
            }
            var _url = appConfig.DOMAIN_URL + appValue.API_URL + 'products?page=' + $page + '&per_page=10';
            if(typeof $url != "undefined"){
                _url += $url;
            }
            return $http({
                method: 'GET',
                url: _url
            }).then(function(response) {
                $ionicLoading.hide();
                // handle success things
                if(response.data.status === appValue.API_SUCCESS){
                    products = response.data.data;
                    return products;
                }
                else {
                    //handle errors
                    return products;
                }
            },function error(response){
                $ionicLoading.hide();
                var message = $rootScope.appLanguage.NETWORK_OFFLINE_TEXT;
                if( response.data.hasOwnProperty('message') ){
                    message = response.data.message;
                }
                /*$ionicPopup.alert({
                 title: $rootScope.appLanguage.MESSAGE_TEXT,
                 template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                 });*/
                AppService.alert(message, $rootScope.appLanguage.MESSAGE_TEXT);
                return false;
            });
        }
    };
});