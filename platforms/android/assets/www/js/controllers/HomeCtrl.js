Ctrl.controller('HomeCtrl', function($scope, $state, $rootScope, ProductService) {
    var appSettings = JSON.parse(window.localStorage.getItem('appSetting'));

    if( appSettings.hasOwnProperty('banners') ){
        $scope.banners = appSettings.banners;
    }

    $scope.productData = [];
    $scope.pageProduct = 1;
    $scope.canLoadMoreProductData = true;
    $scope.openProduct = function($productId) {
        angular.forEach($scope.productData, function(product, key) {
            if (product.id === $productId) {
                window.localStorage.setItem("product", JSON.stringify(product));
                $state.go('tab.product', {productId: $productId});
                return true;
            }
        });
    };
    $scope.getProductData = function($page) {
        ProductService.getProducts($page).then(function(listProduct) {
            if(!listProduct){
                $scope.canLoadMoreProductData = false;
                return false;
            }

            if (listProduct.length > 0) {
                angular.forEach(listProduct, function(product, key) {
                    $scope.productData.push(product);
                });
                $scope.pageProduct = $page + 1;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                $scope.canLoadMoreProductData = true;
            }
            else {
                $scope.canLoadMoreProductData = false;
            }
        });
    };
    $scope.loadMoreProductData = function() {
        $scope.getProductData($scope.pageProduct);
    };
    $scope.loadMoreProductData();
});