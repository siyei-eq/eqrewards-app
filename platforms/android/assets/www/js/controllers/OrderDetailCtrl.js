Ctrl.controller('OrderDetailCtrl', function($scope) {
    $scope.order = JSON.parse(window.localStorage.getItem("singleOrder"));
    $scope.pointsSubTotal = 0;
    $scope.subTotal = 0;

    angular.forEach($scope.order.detail, function(item, key) {
        $scope.pointsSubTotal += parseFloat(item.points_used);
        $scope.subTotal += parseFloat(item.plus_amount);
    });
});