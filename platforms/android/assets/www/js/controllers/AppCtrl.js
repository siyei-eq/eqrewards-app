Ctrl.controller('AppCtrl', function($scope, $state, $ionicSideMenuDelegate, $location, $sce, $rootScope, $ionicModal,
                                   AppService, UserService, CartService, WishlistService, appConfig, appLanguage,
                                   LanguageService, $ionicPlatform) {
    $scope.isEnableFirstLogin = function() {
        return appConfig.ENABLE_FIRST_LOGIN === true;
    };
    $scope.userInfo = {};

    $scope.loginData = {};
    $scope.registerData = {};
    $scope.productData = {};
    $scope.forgotpassData = {};
    $rootScope.cartQuantity = CartService.getCartQuantity();
    $rootScope.wishlistQuantity = WishlistService.getWishlistQuantity();
    $rootScope.appLanguage = appLanguage[LanguageService.getLanguage()];

    if(!appConfig.ENABLE_PUSH) {
        AppService.updateAppSetting();
    }

    AppService.updateShopsList();

    $scope.doLoginWithGoogle = function() {
        //AuthService.loginWithGoogle($scope);
    };

    $scope.doLoginWithFacebook = function() {
        //AuthService.loginWithFacebook($scope);
    };

    var lastEmailUsedInLoggin = window.localStorage.getItem("last_email_login");
    if( lastEmailUsedInLoggin !== null && lastEmailUsedInLoggin !== undefined ){
        $scope.loginData.user_login = lastEmailUsedInLoggin;
    }

    $scope.isLoggedIn = function() {
        if (UserService.isLoggedIn()) {
            $scope.userInfo = UserService.getUser();
            return true;
        }
        else {
            return false;
        }
    };

    $scope.doLogin = function(loginForm) {
        if (!loginForm.$valid) {
            return false;
        }
        UserService.login($scope.loginData).then(function(response) {
            if(response === true){
                $scope.closeModalLogin();
            }
        });
    };

    $scope.doLogout = function() {
        UserService.logout();
        CartService.clearCart();
        //if($scope.isEnableFirstLogin() === true && $scope.isLoggedIn() === false) {
            $scope.mLogin.show();
        //}
    };

    $scope.toggleLeftSideMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.trustAsHtml = function(string) {
        return $sce.trustAsHtml(string);
    };

    $scope.openCategory = function() {
        $state.go('tab.categories');
        return true;
    };

    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/login.html', {
        scope: $scope,
        backdropClickToClose: false,
        hardwareBackButtonClose: false
    }).then(function(modal) {
        $scope.mLogin = modal;
    });

    $scope.openModalLogin = function() {
        $scope.mLogin.show();
    };

    $scope.closeModalLogin = function() {
        if($scope.isEnableFirstLogin() === false || $scope.isLoggedIn() === true) {
            $scope.mLogin.hide();
            angular.element("#homeTab").removeClass("hide");
        }
    };

    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/register.html', {
        scope: $scope,
        backdropClickToClose: false,
        hardwareBackButtonClose: false
    }).then(function(modal) {
        $scope.mRegister = modal;
    });

    $scope.openModalRegister = function() {
        $scope.mRegister.show().then(function(){
            $scope.mLogin.hide();
        });
    };

    $scope.closeModalRegister = function() {
        $scope.mLogin.show().then(function(){
            $scope.mRegister.hide();
        });
    };
    // End Modal Register

    // Start Modal Forgot Password
    $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/forgot-password.html', {
        scope: $scope,
        backdropClickToClose: false,
        hardwareBackButtonClose: false
    }).then(function(modal) {
        $scope.mForgotPassword = modal;
    });

    $scope.openModalForgotPassword = function() {
        $scope.mForgotPassword.show().then(function(){
            $scope.mLogin.hide();
        });
    };

    $scope.closeModalForgotPassword = function() {
        $scope.mLogin.show().then(function(){
            $scope.mForgotPassword.hide();
        });
    };

    //login action
    $scope.doForgotpass = function(forgotpassForm) {
        if (!forgotpassForm.$valid) {
            return false;
        }
        UserService.forgotpass($scope.forgotpassData).then(function(response) {
            $scope.closeModalForgotPassword();
        });

    };

    $scope.doRegister = function(registerForm) {
        if (!registerForm.$valid || $scope.registerData.user_pass !== $scope.registerData.user_confirmpass) {
            return false;
        }
        $scope.registerData.user_login = $scope.registerData.user_email;
        UserService.register($scope.registerData).then(function(response) {
            if(response === true){
                $scope.closeModalRegister();
            }
        });
    };

    $scope.getUserDisplay = function() {
        var user = UserService.getUser();
        return user.name;
    };

    $scope.$on("$ionicView.loaded", function(event, data) {
        //something before enter the view
        $ionicModal.fromTemplateUrl('templates/'+appConfig.ENABLE_THEME+'/modal/login.html', {
            scope: $scope,
            backdropClickToClose: false,
            hardwareBackButtonClose: false
        }).then(function(modal) {
            if($scope.isEnableFirstLogin() === true && $scope.isLoggedIn() === false) {
                $scope.mLogin.show();
            } else if( UserService.getAccessToken() === undefined ||
                UserService.getAccessToken() === "" || UserService.getAccessToken() === null ){
                $scope.mLogin.show();
            } else {
                //al principio oculto el contenido porque el login es modal
                angular.element("#homeTab").removeClass("hide");
            }
        });
    });

    $scope.$on('updateCartQuantity', function (event, data) {
        CartService.updateCartQuantity(data);
        $rootScope.cartQuantity = CartService.getCartQuantity();
    });

    $ionicPlatform.registerBackButtonAction(function (event) {
        if($state.current.name=="home"){
            console.log("button back");
        }
    }, 100);

});