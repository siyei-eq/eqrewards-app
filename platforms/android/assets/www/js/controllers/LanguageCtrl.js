Ctrl.controller('LanguageCtrl', function($scope, $state, $ionicModal, $rootScope, AppService,
                                        listLanguage, appLanguage, LanguageService) {
    $scope.language = LanguageService.getLanguage();
    $scope.listLanguage = listLanguage;

    $scope.changeLanguage = function(language) {
        LanguageService.saveLanguage(language);
        $rootScope.appLanguage = appLanguage[LanguageService.getLanguage()];
    };
});