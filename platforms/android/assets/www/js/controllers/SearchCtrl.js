Ctrl.controller('SearchCtrl', function($scope, $state, $ionicPopover, appConfig, appValue,
                                      ProductService, ListCategoryService) {
    $scope.pageSearch = 1;
    $scope.searchData = [];
    $scope.searchFormData = {};
    $scope.canLoadMoreSearchData = false;
    $scope.searchFormData.category = 'all';

    $scope.openProduct = function($productId) {
        angular.forEach($scope.searchData, function(product, key) {
            if (product.id === $productId) {
                window.localStorage.setItem("product", JSON.stringify(product));
                $state.go('tab.product', {productId: $productId});
                return true;
            }
        });
    };

    $scope.categoryData = [];
    ListCategoryService.getListCategory().then(function(listCategory) {
        $scope.categoryData = listCategory;
    });

    $scope.getSearchData = function($page) {
        ProductService.getProducts($page, '&type=search&param=' + $scope.searchFormData.keyword)
            .then(function(listProduct) {
                if(!listProduct){
                    $scope.canLoadMoreSearchData = false;
                    return false;
                }

                if (listProduct.length > 0) {
                    angular.forEach(listProduct, function(product, key) {
                        $scope.searchData.push(product);
                    });
                    $scope.pageSearch = $page + 1;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.$broadcast('scroll.refreshComplete');
                    //$scope.canLoadMoreSearchData = true;
                }
                else {
                    //$scope.canLoadMoreSearchData = false;
                }
            })

        ;
    };
    $scope.doSearch = function() {
        $scope.pageSearch = 1;
        $scope.searchData = [];
        $scope.canLoadMoreSearchData = false;
        if($scope.searchFormData.keyword) {
            $scope.getSearchData(1);
        }
    };

    $scope.loadMoreSearchData = function() {
        $scope.getSearchData($scope.pageSearch);
    };
});