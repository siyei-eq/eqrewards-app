Ctrl.controller('CheckoutNoteCtrl', function($scope, $state, $ionicPopup, $rootScope, AppService, UserService, OrderService) {
    $scope.userInfo = UserService.getUser();
    $scope.orderInfo = {};
    $scope.orderInfo.orderNote = "";
    $scope.listCountry = {};
    AppService.getListCountry().then(function(listCountry){
        $scope.listCountry = listCountry;
    });
    $scope.listState = {};
    $scope.changeCountry = function() {
        //remove state default
        $scope.userInfo.shipping.state = '';
    };
    $scope.checkCountryHasState = function() {
        var result = false;
        angular.forEach($scope.listCountry, function(country, key) {
            if (country.id === $scope.userInfo.shipping.country) {
                if(country.state.length !== 0) {
                    $scope.listState = country.state;
                    result = true;
                }
            }
        });
        return result;
    };
    $scope.nextStep = function(){
        OrderService.updateOrderInfoShipping($scope.userInfo.shipping);
        OrderService.updateOrderInfoCustomerNote($scope.orderInfo.orderNote);
        if(OrderService.validateOrderInfoShipping()) {
            UserService.updateUser($scope.userInfo);
            $state.go('tab.checkout-payment');
        }
        else {
            $ionicPopup.alert({
                title: $rootScope.appLanguage.MESSAGE_TEXT,
                template: $rootScope.appLanguage.REQUIRED_ERROR_TEXT
            });
        }
    };
});
