Ctrl.controller('NotificationCtrl', function($scope, AppService) {
    $scope.notification = JSON.parse(window.localStorage.getItem("appNotificationPayload"));
    if($scope.notification.type === 'text'){
        $scope.notification.content = $scope.notification.content.replace(/\\"/g, '"');
    }
});