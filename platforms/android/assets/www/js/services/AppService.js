Srv.factory('AppService', function($state, $ionicLoading, $http, $rootScope, $ionicPopup, appConfig, appValue) {
    var appSetting = {'thousand_separator': ',', 'decimal_separator': '.', 'number_decimals': 2};
    var listCountry = [];
    var listShops = [];
    var hasNotification = false;
    var disableApp = false;
    return {
        getDisableApp: function() {
            return disableApp;
        },
        getListCountry: function() {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders?task=list_countries'
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        listCountry = response.data.data;
                        return listCountry;
                    }
                    else {
                        return listCountry;
                    }
                }, function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                })
                ;
        },
        updateAppSetting: function() {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            var deviceToken = window.localStorage.getItem("deviceToken");
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'settings?token=' + deviceToken
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        appSetting = response.data.data;
                        if(appSetting.disable_app && appSetting.disable_app === "1"){
                            $ionicPopup.alert({
                                title: $rootScope.appLanguage.MAINTAIN_TEXT,
                                template: appSetting.disable_app_message
                            });
                            disableApp = true;
                        }
                        window.localStorage.setItem("appSetting", JSON.stringify(appSetting));
                        return appSetting;
                    }
                    else {
                        window.localStorage.setItem("appSetting", JSON.stringify(appSetting));
                        return appSetting;
                    }
                }, function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                });
        },
        getAppSetting: function() {
            if(window.localStorage.getItem("appSetting")){
                return  JSON.parse(window.localStorage.getItem("appSetting"));
            }
            else {
                return {};
            }
        },
        updateShopsList: function(){

            var timeDiff, diffDays, last_check = window.localStorage.getItem("last_check");
            if( last_check !== "" ) {
                timeDiff = ((new Date()).getTime() - (new Date(last_check)).getTime());
                diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
                if ( diffDays <= 0 ){
                    return false;
                }
            }
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'shops'
            }).then(function(response) {
                $ionicLoading.hide();
                // handle success things
                if(response.data.status === appValue.API_SUCCESS){
                    listShops = response.data.data;
                    window.localStorage.setItem("appShops", JSON.stringify(listShops));
                    window.localStorage.setItem("last_check", new Date());
                    return listShops;
                }
                else {
                    return listShops;
                }
            }, function error(response){});
        },
        getShopsList: function(){
            if(window.localStorage.getItem("appShops")){
                return  JSON.parse(window.localStorage.getItem("appShops"));
            }
            else {
                return {};
            }
        }
    };
});