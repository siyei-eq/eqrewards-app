Srv.factory('ListCategoryService', function($ionicLoading, $ionicPopup, $rootScope, $http, appConfig, appValue) {

    var listCategory = [];
    return {
        getListCategory: function() {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'categories?per_page=all'
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        listCategory = response.data.data;
                        return listCategory;
                    }
                    else {
                        //handle errors
                        return listCategory;
                    }
                },function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return listCategory;
                });
        }
    };
});