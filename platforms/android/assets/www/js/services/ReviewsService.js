Srv.factory('ReviewsService', function($ionicLoading, $http, appConfig, appValue) {

    var reviews = [];
    var submitReviewStatus = false;
    return {
        getProductReviews: function($productId) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'reviews?id='+$productId
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        reviews = response.data.data;
                        return reviews;
                    }
                    else {
                        //handle errors
                    }
                });
        },
        submitProductReview: function($productId, $userId, $first_name, $last_name, $email, $comment, $rating) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'reviews?task=add',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    'id': $productId,
                    'user_id': $userId,
                    'user_login': $first_name + ' ' + $last_name,
                    'user_email': $email,
                    'comment': $comment,
                    'rating': $rating
                }
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        submitReviewStatus = true;
                        return submitReviewStatus;
                    }
                    else {
                        return submitReviewStatus;
                    }
                });
        }
    };
});