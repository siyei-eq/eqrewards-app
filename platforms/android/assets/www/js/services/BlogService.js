Srv.factory('BlogService', function($ionicLoading, $ionicPopup, $rootScope, $http, appConfig, appValue) {

    var listBlog = [];
    return {
        getListBlog: function($page, $perpage) {
            $page = $page ? $page : 1;
            $perpage = $perpage ? $perpage : 10;

            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'news?page=' + $page + '&per_page=' + $perpage
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        listBlog = response.data.data;
                        return listBlog;
                    }
                    else {
                        //handle errors
                        return listBlog;
                    }
                },function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return listBlog;
                });
        },
        getListBlogByCategory: function($categoryId, $page, $perpage) {
            $page = $page ? $page : 1;
            $perpage = $perpage ? $perpage : 10;

            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'blogs?type=category&param=' + $categoryId + '&page=' + $page + '&per_page=' + $perpage
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        return response.data.data;
                    }
                    else {
                        //handle errors
                        return [];
                    }
                },function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return [];
                });
        },
        getListBlogCategory: function() {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'blogs?type=get_category'
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        return response.data.data;
                    }
                    else {
                        //handle errors
                        return [];
                    }
                },function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return [];
                });
        },
        getSingleBlog: function($blogId) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'news/' + $blogId
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        return response.data.data;
                    }
                    else {
                        //handle errors
                        return [];
                    }
                },function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return [];
                });
        },
        submitComment: function($blogId, $userId, $name, $email, $comment) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'blogs?type=add&param=' + $blogId,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    'user_id': $userId,
                    'user_login': $name,
                    'user_email': $email,
                    'comment': $comment
                }
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS){
                        $ionicPopup.alert({
                            title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: 'Your comment is awaiting approval'
                        });
                        return true;
                    } else {
                        $ionicPopup.alert({
                            title: $rootScope.appLanguage.MESSAGE_TEXT,
                            template: response.data.message
                        });
                        return false;
                    }
                });
        }
    };
});