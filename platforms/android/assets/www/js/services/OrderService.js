Srv.factory('OrderService', function($ionicLoading, $http, $ionicPopup, $rootScope, AppService, appConfig, appValue, UserService) {
    var listOrder = [];
    var orderInfo = {};
    var orderReceivedInfo = {};
    var orderDiscountCost = 0;
    var orderGrandTotal = 0;
    var orderCurrency = 'USD';
    return {
        updateServerCartId: function($cartId) {
            orderInfo.cart_id = $cartId;
        },
        updateOrderReceivedInfo: function($orderReceivedInfo) {
            orderReceivedInfo = $orderReceivedInfo;
        },
        clearOrderInfo: function() {
            orderInfo = {};
        },
        getOrderCurrency: function() {
            return orderCurrency;
        },
        setOrderGrandTotal: function($orderGrandTotal) {
            orderGrandTotal = $orderGrandTotal;
        },
        getOrderGrandTotal: function() {
            return orderGrandTotal;
        },
        getOrderDiscountCost: function() {
            return orderDiscountCost;
        },
        getListOrder: function(token, $page){
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                headers: {'Authorization' : token },
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders?&per_page=10&page='+$page
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        listOrder = response.data.data;
                        return listOrder;
                    }
                    else {
                        //handle errors
                    }
                });
        },
        getOrderDetail: function($orderid) {

        },
        createServeCart: function($coupon, $products, $country, $state, $postcode){
            //add product in cart to order
            var line_items = [];
            angular.forEach($products, function(product, key) {
                if(product[2].product_id !== undefined){
                    line_items.push({product_id: product[2].product_id, product_name: product[2].name, variation_id: product[0], quantity: product[1]});
                }
                else {
                    line_items.push({product_id: product[0], product_name: product[2].name, quantity: product[1]});
                }
            });
            this.updateOrderInfoLineItems(line_items);
            this.updateOrderInfoCoupon($coupon);
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders/prepare',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: this.getOrderInfo()
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        //update order grand total
                        orderGrandTotal = parseFloat(response.data.data.price.total);
                        //update order currency
                        orderCurrency = response.data.data.price.currency;
                        //update order discount cost
                        orderDiscountCost = response.data.data.price.discount_total;
                        //update list payment
                        var listpayment = [];
                        listpayment = response.data.data.payment_methods;
                        var listzones = [];
                        listzones = response.data.data.shipping_methods;
                        var listshipping = [];
                        angular.forEach(listzones.zones, function(zone, key) {
                            if(zone.zone_locations.indexOf($country) >= 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($state) >= 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($postcode) >= 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($country+':'+$state) >= 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($country+'-'+$postcode) >= 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($country+':'+$state+'-'+$postcode) >= 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                        });
                        if(listshipping.length === 0 ){
                            //find shipping method at default zone
                            listshipping = listzones.default.shipping_methods;
                        }
                        var result = {};
                        result.shipping_methods = listshipping;
                        result.payment_methods = listpayment;
                        result.server_cart_id = response.data.data.cart_id;
                        return result;
                    }
                    else {
                        //handle errors
                    }
                });
        },
        getPaymentMethod: function(){
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders?task=list_payment'
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        var listpayment = [];
                        listpayment = response.data.data;
                        return listpayment;
                    }
                    else {
                        //handle errors
                    }
                });
        },
        getShippingMethod: function($country, $state, $postcode){
            $ionicLoading.show({
                template: 'Loading...'
            });
            return $http({
                method: 'GET',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders?task=list_shipping'
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if (response.data.status === appValue.API_SUCCESS) {
                        var listzones = [];
                        listzones = response.data.data;
                        var listshipping = [];
                        angular.forEach(listzones.zones, function(zone, key) {
                            if(zone.zone_locations.indexOf($country) > 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($state) > 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($postcode) > 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($country+':'+$state) > 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                            if(zone.zone_locations.indexOf($country+':'+$state+'-'+$postcode) > 0){
                                listshipping = zone.shipping_methods;
                                return listshipping;
                            }
                        });
                        if(listshipping.length === 0 ){
                            //find shipping method at default zone
                            listshipping = listzones.default.shipping_methods;
                        }
                        return listshipping;
                    }
                    else {
                        //handle errors
                    }
                });
        },
        getOrderInfo: function() {
            return orderInfo;
        },
        getOrderReceivedInfo: function() {
            return orderReceivedInfo;
        },
        updateOrderInfoCustomerId: function($customerId) {
            orderInfo.customer_id = $customerId;
        },
        updateOrderInfoBilling: function($billingInfo) {
            orderInfo.billing = JSON.stringify($billingInfo);
        },
        updateOrderInfoShipping: function($shippingInfo) {
            orderInfo.shipping = JSON.stringify($shippingInfo);
        },
        updateOrderInfoCustomerNote: function($customerNote) {
            orderInfo.customer_note = $customerNote;
        },
        updateOrderInfoCoupon: function($coupon) {
            orderInfo.coupon = $coupon;
        },
        updateDeviceToken: function($token){
            orderInfo.device_token = $token;
        },
        updateOrderInfoPaymentMethod: function($payment, $title, $data) {
            orderInfo.payment_method = $payment;
            orderInfo.payment_method_title = $title;
            orderInfo.payment_method_data = $data;
        },
        validateOrderInfoBilling: function() {
            var billing = JSON.parse(orderInfo.billing);
            if( billing.name !== "" && billing.phone !== ""  && billing.city !== ""  && billing.address !== "" &&
                billing.name !== undefined && billing.phone !== undefined
                && billing.city !== undefined  && billing.address !== undefined) {
                return true;
            }
            return false;
        },
        updateOrderInfoShippingMethod: function($shipping_lines) {
            orderInfo.shipping_lines = JSON.stringify($shipping_lines);
        },
        updateOrderInfoLineItems: function($line_items) {
            orderInfo.line_items = JSON.stringify($line_items);
        },
        getOrderInfoLineItems: function() {
            return JSON.parse(orderInfo.line_items);
        },
        validateOrderInfoShipping: function() {
            var shipping = JSON.parse(orderInfo.shipping);
            if(shipping.first_name !== "" && shipping.last_name !== "" && shipping.country !== "" && shipping.address_1 !== "" && shipping.state !== "" && shipping.city !== "" && shipping.postcode !== "") {
                return true;
            }
            return false;
        },
        getCartPrice: function() {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders?task=get_price',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: this.getOrderInfo()
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        //update order grand total
                        orderGrandTotal = response.data.data.total;
                        //update order currency
                        orderCurrency = response.data.data.currency;
                        //update order discount cost
                        orderDiscountCost = response.data.data.discount_total;
                        return true;
                    }
                    else {
                        return false;
                    }
                }, function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return false;
                });
        },
        changeOrderStatus: function($id) {
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders?task=change_order_status',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    'id': $id,
                    'status': 'processing'
                }
            })
                .then(function(response) {
                    $ionicLoading.hide();
                    // handle success things
                    if(response.data.status === appValue.API_SUCCESS){
                        return true;
                    }
                    else {
                        return false;
                    }
                }, function error(response){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: $rootScope.appLanguage.NETWORK_OFFLINE_TEXT
                    });
                    return false;
                });
        },
        createOrder: function() {
            if(AppService.getDisableApp()) {
                $ionicPopup.alert({
                    title: $rootScope.appLanguage.MAINTAIN_TEXT,
                    template: AppService.getAppSetting().disable_app_message
                });
                return true;
            }
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner>'
            });
            return $http({
                method: 'POST',
                url: appConfig.DOMAIN_URL + appValue.API_URL + 'orders/create',
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded',
                    'Authorization': UserService.getAccessToken()
                },
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: this.getOrderInfo()
            }).then(function(response) {
                $ionicLoading.hide();
                // handle success things
                if(response.data.status === appValue.API_SUCCESS){
                    orderReceivedInfo = response.data.data;
                    orderReceivedInfo.payment_method = orderInfo.payment_method;
                    orderReceivedInfo.payment_method_data = orderInfo.payment_method_data;
                    return true;
                } else {
                    $ionicPopup.alert({
                        title: $rootScope.appLanguage.MESSAGE_TEXT,
                        template: response.data.message
                    });
                    return false;
                }
            }, function error(response){
                $ionicLoading.hide();

                if( response.data.hasOwnProperty('message') ){
                    return response.data;
                }
                /*if( response.data.hasOwnProperty('errno') ){
                    //if( response.data.errno === AppService.getAppSetting().out_of_stock ) {
                        console.log("OUTOFSTOCK");
                        return response.data;
                    //} else if( response.data.errno === AppService.getAppSetting().not_enough_points ) {
                }*/
                return false;
            });
        }
    };
});