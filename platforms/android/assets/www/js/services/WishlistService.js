Srv.factory('WishlistService', function($rootScope) {
    var wishlistInfo = {};
    wishlistInfo.products = [];
    if(window.localStorage.getItem("wishlistInfo") && window.localStorage.getItem("wishlistInfo") !== "undefined") {
        wishlistInfo = JSON.parse(window.localStorage.getItem("wishlistInfo"));
    }
    return {
        clearWishlist: function() {
            wishlistInfo = {};
            wishlistInfo.products = [];
            this.updateWishlistInfo();
            $rootScope.wishlistQuantity = this.getWishlistQuantity();
        },
        checkProductInWishlist: function($productId) {
            var isInWishlist = false;
            angular.forEach(wishlistInfo.products, function(product, key) {
                if (product[0] === $productId) {
                    isInWishlist = true;
                }
            });
            return isInWishlist;
        },
        addProductToWishlist: function($productId, $quantity, $info) {
            var quantity = $quantity;
            var isNew = true;
            angular.forEach(wishlistInfo.products, function(product, key) {
                if (product[0] === $productId) {
                    quantity = quantity + product[1];
                    wishlistInfo.products[key][1] = quantity;
                    isNew = false;
                }
            });
            if(isNew === true) {
                var product = [$productId, $quantity, $info];
                wishlistInfo.products.push(product);
            }
            this.updateWishlistInfo();

            return true;
        },
        removeProductFromWishlist: function($productId) {
            angular.forEach(wishlistInfo.products, function(product, key) {
                if (product[0] === $productId) {
                    wishlistInfo.products.splice(key, 1);
                }
            });
            this.updateWishlistInfo();
            $rootScope.wishlistQuantity = this.getWishlistQuantity();
            return true;
        },
        getWishlistInfo: function() {
            return wishlistInfo;
        },
        getWishlistQuantity: function() {
            var total = 0;
            angular.forEach(wishlistInfo.products, function(product, key) {
                total = total + 1;
            });
            return total;
        },
        updateWishlistInfo: function() {
            window.localStorage.setItem("wishlistInfo", JSON.stringify(wishlistInfo));
        }
    };
});